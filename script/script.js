'use strict'
// Solve:
window.onload = () => {

	const divAnswear1 = document.querySelector('.answer-1');
	const buttonCircle = document.getElementById('buttonCircle');

	// Сreate button "Draw"
	const buttonPaint = document.createElement('input');
	buttonPaint.setAttribute('type', 'button');
	buttonPaint.setAttribute('value', 'Нарисовать');
	buttonPaint.setAttribute('id', 'buttonPaint');

	// Create circle diametr
	const input = document.createElement('input');
	input.setAttribute('type', 'text');
	input.setAttribute('value', '');
	input.setAttribute('placeholder', 'Введите диаметр круга');

	// Editor for "Draw the button"
	buttonCircle.onclick = () => {
		// Delete button
		buttonCircle.remove();

		// Add button
		divAnswear1.append(buttonPaint);

		// Add input line
		divAnswear1.append(input);
	}

	// Editor for paint
	buttonPaint.onclick = () => {
		// Get users data
		const diameterCircle = input.value + 'px';
		// Remove button
		buttonPaint.remove();
		input.remove();

		// Draw the circle
		let i = 100;
		while (i != 0) {
			const circle = document.createElement('div');
			circle.setAttribute('class', 'circle');
			circle.style.background = `rgb(${(Math.random() * 255).toFixed(0)}, ${(Math.random() * 255).toFixed(0)}, ${(Math.random() * 255).toFixed(0)})`;
			circle.style.height = diameterCircle;
			circle.style.width = diameterCircle;
			divAnswear1.append(circle);
			i--;
		};
	};

	// Editor when click on the button
	divAnswear1.addEventListener('click', (event) => {
		if (event.target.classList.contains('circle')) {
			event.target.remove();
		};
	})
};


